//
//  ImageCountIn.swift
//  Brandwise Image Downloader
//
//  Created by Keith Weiss on 7/13/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import Foundation

//  MARK: - INPUT
// MARK: - ImageCountIn
struct ImageCountIn: Codable {
    let tsList: TSList
    
    enum CodingKeys: String, CodingKey {
        case tsList = "TSList"
    }
    
    init(vendorList: [VendorTS]) {
        self.tsList = TSList(vendorList: vendorList)
    }
}


// MARK: - TSList
struct TSList: Codable {
    var lasttimestamp: Int64 = 0
    let maxdbts: Int64 = Int64.max
    let vmiVersion: String = "12.2008"
    var statusMsg: String = ""
    var tag: String = ""
    var tablename: String = ""
    var agencyTSList: [AgencyTS] = []
    var vendorTSList: [VendorTS] = []
    let userGUID: String = "F2657196-4A6C-43CD-B740-C97E898AA91C"
    var statusCode: Int = 0
    
    enum CodingKeys: String, CodingKey {
        case lasttimestamp, maxdbts
        case vmiVersion = "_vmiVersion"
        case statusMsg = "_statusMsg"
        case tag = "_tag"
        case tablename, agencyTSList, vendorTSList, userGUID
        case statusCode = "_statusCode"
    }
    
    init(vendorList: [VendorTS]) {
        self.vendorTSList = vendorList
    }
}


// MARK: - VendorTS
struct AgencyTS: Codable {
    let ssiAgencyCorpID: Int64
    var agencyLogoImageLastTimestamp: Int64 = 0
    
    init(corpID: Int64) {
        self.ssiAgencyCorpID = corpID
    }
}


// MARK: - VendorTS
struct VendorTS: Codable {
    let ssiVendorID: Int64
    let attributeIconImageLastTimestamp: Int64 = 0
    let productImageLastTimestamp: Int64 = 0
    let catalogImageLastTimestamp: Int64 = 0
    let marketingImageLastTimestamp: Int64 = 0
    
    init(vendorID: Int64) {
        self.ssiVendorID = vendorID
    }
}
