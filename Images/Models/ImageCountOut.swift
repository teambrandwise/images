//
//  ImageCountOut.swift
//  Brandwise Image Downloader
//
//  Created by Keith Weiss on 7/13/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import Foundation
import SwiftUI

//  MARK: - OUTPUT
protocol ImageLocatable {
    var imageURLPathComponent: URL? { get }
}


enum ImageType: String, Codable {
    case attribute = "BWAttributeIconImageVMi"
    case catalog = "BWCatalogImageVMi"
    case marketing = "BWMarketingImage"
    case product = "BWProductImage"
    
    var header: String {
        let headers = ["Attribute", "Catalog", "Marketing", "Product"]
        switch self {
        case .attribute:
            return headers[0]
        case .catalog:
            return headers[1]
        case .marketing:
            return headers[2]
        case .product:
            return headers[3]
        }
    }
}

enum PrimaryKey: String, Codable {
    case imageName = "imageName"
    case ssiVendorID = "ssiVendorID"
}


// MARK: - ImageCountOut
struct ImageCountOut: Codable, CustomStringConvertible {
    let deletedRowsIncluded: Bool
    let deviceShowMode: Bool
    let resetForFull: Bool
    let transactionID: Int64
    let maxTS: Int64
    let statusCode: Int
    var statusMsg: String?
    let tag: String
    var agencyData: [AgencyDatum]?
    var vendorData: [VendorDatum]?
    
    enum CodingKeys: String, CodingKey {
        case deletedRowsIncluded = "_deletedRowsIncluded"
        case deviceShowMode = "_deviceShowMode"
        case maxTS = "_maxTS"
        case resetForFull = "_resetForFull"
        case statusCode = "_statusCode"
        case statusMsg = "_statusMsg"
        case tag = "_tag"
        case transactionID = "_transactionID"
        case agencyData, vendorData
    }
    
    init(_ statusMessage: String) {
        self.statusMsg = statusMessage
        deletedRowsIncluded = false
        deviceShowMode = false
        resetForFull = false
        
        transactionID = Int64.max
        maxTS = Int64.max
        statusCode = -86
        tag = "Default"
    }
    
    static var `default` = ImageCountOut("Default")
    
    var description: String {
        var results = ""
        if let agencyData = agencyData {
            results += "\n-> AGENCY: \(agencyData)"
        }
        if let vendorData = vendorData {
            results += "\n-> VENDOR: \(vendorData)"
        }
        if results.isEmpty {
            results = "NO AGENCY OR VENDOR DATA"
        }
        return results
    }
}


// MARK: - AgencyDatum
struct AgencyDatum: Codable, CustomStringConvertible {
    let agencyImage: Images?
    let totalAgencyLogoImageCount: Int
    let agencyLogoImageLastModifiedDate: Date
    
    let agencyName: String
    let ssiAgencyCorpID: Int64
    let ssiAgencyID: Int64
    
    
    enum CodingKeys: String, CodingKey {
        case agencyImage, totalAgencyLogoImageCount
        case agencyLogoImageLastModifiedDate
        case agencyName, ssiAgencyCorpID, ssiAgencyID
    }
    
    var description: String {
        var results = "Agency: \(agencyName)  [\(ssiAgencyID)]  CorpID: \(ssiAgencyCorpID)"
        results += "\n   IMAGES: "
        if let images = agencyImage {
            results += "\n-> AGENCY: \(images)"
        }
        else {
            results += "NO AGENCY IMAGES"
        }
        return results
    }
}


// MARK: - VendorDatum
struct VendorDatum: Codable, CustomStringConvertible, Equatable, Hashable, Identifiable {
    static func == (lhs: VendorDatum, rhs: VendorDatum) -> Bool {
        lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
         hasher.combine(id)
    }
    
    
    let attributeImages: Images?
    let totalAttributeIconImageCount: Int
    let attributeIconImageLastModifiedDate: Date
    
    let catalogImages: Images?
    let totalCatalogImageCount: Int
    let catalogImageLastModifiedDate: Date
    
    let marketingImages: Images?
    let totalMarketingImageCount: Int
    let marketingImageLastModifiedDate: Date
    
    let productImages: Images?
    let totalProductImageCount: Int
    let productImageLastModifiedDate: Date
    
    let corpID: Int64
    var id: Int64
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case id = "ssiVendorID"
        case corpID = "ssiVendorCorpID"
        case attributeImages, totalAttributeIconImageCount, attributeIconImageLastModifiedDate
        case catalogImages, totalCatalogImageCount, catalogImageLastModifiedDate
        case marketingImages, totalMarketingImageCount, marketingImageLastModifiedDate
        case productImages, totalProductImageCount, productImageLastModifiedDate
        case name = "vendorName"
    }
    
    var description: String {
        var results = "Vendor: \(name) [\(id)]"
        results += "\n   IMAGES: "
        var imageCount = 0
        if let images = attributeImages {
            imageCount += images.images?.count ?? 0
            results += "\n-> ATTRIBUTE: \(images)"
        }
        if let images = catalogImages {
            imageCount += images.images?.count ?? 0
            results += "\n-> CATALOG: \(images)"
        }
        if let images = marketingImages {
            imageCount += images.images?.count ?? 0
            results += "\n-> MARKETING: \(images)"
        }
        if let images = productImages {
            imageCount += images.images?.count ?? 0
            results += "\n-> PRODUCT: \(images)"
        }
        if imageCount == 0 {
            results += "NO IMAGES RETURNED"
        }
        return results
    }
}


// MARK: - Images
struct Images: Codable, CustomStringConvertible {
    let maxTS: Int64
    let resetForFull: Bool
    let statusCode: Int
    let statusMsg: String?
    let tag: String
    let transactionID: Int64
    let deletedImages: [ClientImage]?
    let images: [ClientImage]?
    
    enum CodingKeys: String, CodingKey {
        case maxTS = "_maxTS"
        case resetForFull = "_resetForFull"
        case statusCode = "_statusCode"
        case statusMsg = "_statusMsg"
        case tag = "_tag"
        case transactionID = "_transactionID"
        case deletedImages, images
    }
    
    var description: String {
        var results = ""
        if let images = images {
            results += "\n    IMG: \(images)\n"
        }
        else {
            results += "\n    IMG: NONE\n"
        }
        if let images = deletedImages {
            results += "\n    DEL: \(images)\n"
        }
        else {
            results += "\n    DEL: NONE\n"
        }
        return results
    }
}


// MARK: - Image from Brandwise Clients

protocol Insertable {
    var insertDictionary: [String: Any] { get }
}

struct ClientImage: Codable, CustomStringConvertible, Identifiable, ImageLocatable, Insertable {
    var id: UUID? = UUID()
    
    var imageType: ImageType?
    var shouldDelete: Bool?
    var imageFileSize: Int?
    let timeStamp: Int64
    let vendorID: Int64
    let corpID: Int64
    var catalogID: Int64?
    let name: String
    var sku: String?
    
    enum CodingKeys: String, CodingKey {
        case imageType = "_entityName"
        case shouldDelete = "_deleted"
        case imageFileSize
        case timeStamp = "ts"
        case vendorID = "ssiVendorID"
        case corpID = "ssiVendorCorpID"
        case catalogID
        case name = "imageName"
        case sku = "productNumber"
    }
    
    var description: String {
        guard let pathComponent = imageURLPathComponent?.absoluteString else { return "\n            \(id?.uuidString ?? "No ID")" }
        return "\n          path: \(FileManager.assetsHost!.appendingPathComponent(pathComponent).absoluteString)"
    }
    
    var insertDictionary: [String : Any] {
        return [
            "catalogID": NSNumber(value: (catalogID ?? Int64(0))),
            "corpID": NSNumber(value: corpID),
            "imageType": imageType?.rawValue ?? "",
            "name": name,
            "shouldDelete": NSNumber(booleanLiteral: shouldDelete ?? false),
            "sku": sku ?? "",
            "timeStamp": NSNumber(value: timeStamp),
            "vendorID": NSNumber(value: vendorID)
        ]
    }
    
    var imageURLPathComponent: URL? {
        switch imageType {
        case .attribute:
            return FileManager.default.pathURL(attributeImageName: name, vendorID: vendorID, corpID: corpID)
        case .catalog:
            return FileManager.default.pathURL(catalogImageName: name, catalogID: catalogID!, vendorID: vendorID, corpID: corpID)
        case .marketing:
            return FileManager.default.pathURL(marketingImageName: name, vendorID: vendorID, corpID: corpID)
        case .product:
            return FileManager.default.pathURL(productImageName: name, vendorID: vendorID, corpID: corpID)
        default:
            return FileManager.default.pathURL(imageName: name, vendorID: vendorID, corpID: corpID)
        }
    }
}
