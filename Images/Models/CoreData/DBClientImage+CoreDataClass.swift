//
//  DBClientImage+CoreDataClass.swift
//  Images
//
//  Created by Keith Weiss on 7/18/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//
//

import Foundation
import CoreData
import SwiftUI

@objc(DBClientImage)
public class DBClientImage: NSManagedObject, Identifiable {
    public var id: String {
        return name
    }
}
