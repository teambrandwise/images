//
//  DBClientImage+CoreDataProperties.swift
//  Images
//
//  Created by Keith Weiss on 7/18/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//
//

import Foundation
import CoreData


extension DBClientImage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DBClientImage> {
        return NSFetchRequest<DBClientImage>(entityName: "DBClientImage")
    }

    @NSManaged public var catalogID: Int64
    @NSManaged public var corpID: Int64
    @NSManaged public var imageType: String?
    @NSManaged public var name: String?
    @NSManaged public var shouldDelete: Bool
    @NSManaged public var sku: String?
    @NSManaged public var timeStamp: Int64
    @NSManaged public var vendorID: Int64

}
