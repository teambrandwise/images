//
//  DBClientImage+CoreDataProperties.swift
//  Images
//
//  Created by Keith Weiss on 7/18/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//
//

import Foundation
import CoreData
import SwiftUI


extension DBClientImage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DBClientImage> {
        return NSFetchRequest<DBClientImage>(entityName: "DBClientImage")
    }
    
    @NSManaged public var catalogID: Int64
    @NSManaged public var corpID: Int64
    @NSManaged public var imageType: String
    @NSManaged public var name: String
    @NSManaged public var shouldDelete: Bool
    @NSManaged public var sku: String?
    @NSManaged public var timeStamp: Int64
    @NSManaged public var vendorID: Int64

}


//  MARK: - URL MANAGEMENT

extension DBClientImage: ImageLocatable {
    var imageURLPathComponent: URL? {
        guard let imageType = ImageType(rawValue: imageType) else { return nil }
        
        switch imageType {
        case .attribute:
            return FileManager.default.pathURL(attributeImageName: name, vendorID: vendorID, corpID: corpID)
        case .catalog:
            return FileManager.default.pathURL(catalogImageName: name, catalogID: catalogID, vendorID: vendorID, corpID: corpID)
        case .marketing:
            return FileManager.default.pathURL(marketingImageName: name, vendorID: vendorID, corpID: corpID)
        case .product:
            return FileManager.default.pathURL(productImageName: name, vendorID: vendorID, corpID: corpID)
        }
    }
    
    var serverURL: URL {
        let serverURL = FileManager.assetsHost!.appendingPathComponent(imageURLPathComponent!.absoluteString)
        return serverURL
    }
    
    
    var cacheURL: URL {
        let cacheURL = FileManager.cacheHost.appendingPathComponent(imageURLPathComponent!.absoluteString)
        return cacheURL
    }
    
    
    //  MARK: SIZE CGImage to a cell size.
    static func gcImage(_ anImage: UIImage?, to size: Int) -> CGImage {
        let defaultCGImage = UIImage(systemName: "photo")!.cgImage!
        
        guard let image: CGImage = anImage?.cgImage else { return defaultCGImage }
        guard let components = image.colorSpace?.numberOfComponents,
            components > 1 else { return defaultCGImage }
        guard
            let colorSpace = image.colorSpace,
            let context = CGContext(
                data: nil,
                width: size, height: size,
                bitsPerComponent: image.bitsPerComponent,
                bytesPerRow: image.bytesPerRow,
                space: colorSpace,
                bitmapInfo: image.bitmapInfo.rawValue)
            else { return defaultCGImage }
        
        context.interpolationQuality = .high
        context.draw(image, in: CGRect(x: 0, y: 0, width: size, height: size))
        
        if let sizedImage = context.makeImage() {
            return sizedImage
        } else {
            fatalError("Couldn't resize image.")
        }
    }
}
