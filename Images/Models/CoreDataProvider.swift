//
//  CoreDataProvider.swift
//  Brandwise Image Downloader
//
//  Created by Keith Weiss on 7/16/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import SwiftUI
import Combine
import CoreData

final class CoreDataProvider {
    
    enum PersistError: Error, LocalizedError {
        case itFailed
        
        var errorDescription: String {
            return "Persist Error: general error"
        }
    }
    
    static func images(for imageType: ImageType) -> [DBClientImage] {
        let context = APP_DELEGATE.mainContext
        let fetchRequest: NSFetchRequest<DBClientImage> = DBClientImage.fetchRequest()
        fetchRequest.fetchBatchSize = 20
        fetchRequest.predicate = NSPredicate(format: "imageType = %@", imageType.rawValue)
        
        guard let images = try? context.fetch(fetchRequest) else { return [DBClientImage]() }
        return images
    }
    
    
    static func extractImageLists(from vendor: VendorDatum) {
        var dictionaries: [[String : Any]] = []
        dictionaries = (vendor.attributeImages?.images ?? [ClientImage]()).compactMap { $0.insertDictionary }
        self.persist(images: dictionaries, for: .attribute)
            
        dictionaries = (vendor.catalogImages?.images ?? [ClientImage]()).compactMap { $0.insertDictionary }
        self.persist(images: dictionaries, for: .catalog)
        
        dictionaries = (vendor.marketingImages?.images ?? [ClientImage]()).compactMap { $0.insertDictionary }
        self.persist(images: dictionaries, for: .marketing)
        
        dictionaries = (vendor.productImages?.images ?? [ClientImage]()).compactMap { $0.insertDictionary }
        self.persist(images: dictionaries, for: .product)
    }
    
    
    fileprivate static func persist(images: [[String : Any]], for imageType: ImageType) {
        guard !images.isEmpty else { return }
        APP_DELEGATE.persistentContainer.performBackgroundTask { (backgroundContext) in
            do {
                let insertRequest = NSBatchInsertRequest(entity: DBClientImage.entity(), objects: images)
                let insertResult = try backgroundContext.execute(insertRequest) as? NSBatchInsertResult
                guard let success = insertResult?.result as? Bool else { throw PersistError.itFailed }
                if success {
                    try backgroundContext.save()
                    NotificationCenter.default.post(name: Notification.Name.DidUpdateCoreData, object: nil, userInfo: ["imageType": imageType.rawValue])
                }
            }
            catch {
                fatalError("Something bad happened. \(error)")
            }
        }
    }
}
