//
//  ImageProvider.swift
//  Images
//
//  Created by Keith Weiss on 7/18/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import SwiftUI
import Combine
import CoreData

public class ImageProvider: ObservableObject {
    let backgroundQueue = DispatchQueue(label: "com.brandwise.images.background", qos: .background)
    
    var service: ClientImagesService
    var vendorPublisher: AnyPublisher<ImageCountOut, Never>
    
    var hasData: Bool {
        var count = attributeImages.count
        count += catalogImages.count
        count += marketingImages.count
        count += productImages.count
        return count > 0
    }
    
    @Published var vendors: [VendorDatum] = []
    @Published var uiImage: UIImage = UIImage(systemName: "photo.fill")!
    
    @Published var attributeImages: [DBClientImage] = []
    @Published var catalogImages: [DBClientImage] = []
    @Published var marketingImages: [DBClientImage] = []
    @Published var productImages: [DBClientImage] = []
    
    var imageCancellable: AnyCancellable?
    private var coreDataListener: NSObjectProtocol? = .none
    
    deinit {
        if let listener = coreDataListener {
            NotificationCenter.default.removeObserver(listener)
            coreDataListener = .none
        }
        cancelImage()
    }
    
    
    init() {
        service = ClientImagesService()
        vendorPublisher = service.vendorListPublisher
        coreDataListener = NotificationCenter.default.addObserver(forName: Notification.Name.DidUpdateCoreData, object: nil, queue: OperationQueue.main, using: { (notification) in
            if let imageType = ImageType(rawValue: notification.userInfo?["imageType"] as! String) {
                switch imageType {
                case .attribute:
                    self.attributeImages = CoreDataProvider.images(for: imageType)
                case .catalog:
                    self.catalogImages = CoreDataProvider.images(for: imageType)
                case .marketing:
                    self.marketingImages = CoreDataProvider.images(for: imageType)
                case .product:
                    self.productImages = CoreDataProvider.images(for: imageType)
                }
            }
        })
    }
    
    
    func cache(images: [DBClientImage]) {
        let tuples: [(server: URL, cache: URL)] = images.compactMap { (server: $0.serverURL, cache: $0.cacheURL) }
        
        backgroundQueue.async{
            tuples.forEach { (tuple) in
                autoreleasepool {
                    do {
                        if !FileManager.default.fileExists(atPath: tuple.cache.removeFilePrefix) {
                            let data = try Data(contentsOf: tuple.server)
                            FileManager.default.save(filename: tuple.cache, with: data)
                        }
                    }
                    catch {
                        print("CACHING ERROR: Unable to cache \(tuple.server)")
                    }
                }
            }
        }
    }
    
    
    func fetchVendors() {
        let _ = vendorPublisher.sink { imageCountOut in
            guard let vendors = imageCountOut.vendorData else { return }
            self.vendors = vendors
        }
    }
    
    
    func fetchImageCounts(_ vendorID: Int64) {
        let vendorImageRequest = service.imagesRequest(for: vendorID)
        let imageCountPublisher = service.imageCountOut(urlRequest: vendorImageRequest)
        _ = imageCountPublisher.sink { imageCountOut in
            guard let vendors = imageCountOut.vendorData,
                let vendor = vendors.first else { return }
            
            CoreDataProvider.extractImageLists(from: vendor)
        }
    }
}



//  MARK: IMAGE FETCHING EXTENSION

extension ImageProvider {
    func fetchImage(_ url: URL?) {
        guard let url = url else { return }
        cancelImage()
        
        imageCancellable = service.imagePublisher(url: url).sink(receiveValue: { (uiImage) in
             self.uiImage = uiImage
        })
    }
    
    
    func cancelImage() {
        guard let cancellable = imageCancellable else { return }
        
        cancellable.cancel()
    }
}


//  MARK: NOTIFICATIONS

extension Notification.Name {
    static let DidUpdateCoreData = Notification.Name("DidUpdateCoreData")
}
