//
//  FileManager+Extension.swift
//  Brandwise Image Downloader
//
//  Created by Keith Weiss on 7/13/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import Foundation

//  MARK: - EXTENSIONS

//  MARK: - String
extension String {
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
}

//  MARK: - Int64
extension Int64 {
    var formattedSixDigitZeroPadded: String {
        return String(format: "%06i", self)
    }
}


//  MARK: - DateFormatter
extension DateFormatter {
    static let imageCounts: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss'Z'"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
}


//  MARK: - URL
extension URL {
    static func pathComponent(corpID: Int64, vendorID: Int64) -> URL? {
        let pathString = "ApplicationCompany/CorpID_\(corpID.formattedSixDigitZeroPadded)/Vendor/VendorID_\(vendorID.formattedSixDigitZeroPadded)"
        return URL(string: pathString)
    }
    
    
    static func pathComponent(catalogID: Int64, corpID: Int64, vendorID: Int64) -> URL? {
        var path = URL.pathComponent(corpID: corpID, vendorID: vendorID)
        path = path?.appendingPathComponent("Catalogs")
        path = path?.appendingPathComponent("CATALOGID_\(catalogID.formattedSixDigitZeroPadded)")
        return path
    }
    
    var directoryPath: String {
        self.deletingLastPathComponent().absoluteString.deletingPrefix("file://")
    }
    
    var removeFilePrefix: String {
        self.absoluteString.deletingPrefix("file://")
    }
}


//  MARK: FileManager
extension FileManager {
    static let cacheHost: URL = try! FileManager.default.url(for: .documentDirectory,
                                    in: .userDomainMask,
                                    appropriateFor: nil,
                                    create: false)
    static let assetsHost = URL(string: "https://assets.bwconnect.com")
    
    func pathURL(imageName: String, vendorID: Int64, corpID: Int64) -> URL? {
        var returnURL = URL.pathComponent(corpID: corpID, vendorID: vendorID)
        returnURL = returnURL?.appendingPathComponent(imageName)
        return returnURL
    }
    
    
    func pathURL(attributeImageName imageName: String, vendorID: Int64, corpID: Int64) -> URL? {
        var returnURL = URL.pathComponent(corpID: corpID, vendorID: vendorID)
        returnURL = returnURL?.appendingPathComponent("AttributeIcons")
        returnURL = returnURL?.appendingPathComponent(imageName)
        return returnURL
    }
    
    
    func pathURL(catalogImageName imageName: String, catalogID: Int64, vendorID: Int64, corpID: Int64) -> URL? {
        var returnURL = URL.pathComponent(catalogID: catalogID, corpID: corpID, vendorID: vendorID)
        returnURL = returnURL?.appendingPathComponent(imageName)
        return returnURL
    }
    
    
    func pathURL(marketingImageName imageName: String, vendorID: Int64, corpID: Int64) -> URL? {
        var returnURL = URL.pathComponent(corpID: corpID, vendorID: vendorID)
        returnURL = returnURL?.appendingPathComponent("MarketingMaterials")
        returnURL = returnURL?.appendingPathComponent(imageName)
        return returnURL
    }
    
    
    func pathURL(productImageName imageName: String, vendorID: Int64, corpID: Int64) -> URL? {
        var returnURL = URL.pathComponent(corpID: corpID, vendorID: vendorID)
        returnURL = returnURL?.appendingPathComponent("Products")
        returnURL = returnURL?.appendingPathComponent(imageName)
        return returnURL
    }
    
    
    func save(filename: URL, with data: Data) {
        let directory: String = filename.directoryPath
        
        do {
            if !FileManager.default.fileExists(atPath: directory) {
                try FileManager.default.createDirectory(atPath: directory,withIntermediateDirectories: true, attributes: nil)
            }
            try data.write(to: filename, options: .atomicWrite)
        } catch {
            // failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
            print("=+=+=+=+> Failed to write \(filename.lastPathComponent)")
            print("--------> Error: \(error.localizedDescription)")
        }
    }
}
