//
//  UIApplication+Extension.swift
//  Brandwise Image Downloader
//
//  Created by Keith Weiss on 7/16/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import UIKit

var APP_DELEGATE: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
