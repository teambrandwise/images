//
//  VendorImagesList.swift
//  Brandwise Image Downloader
//
//  Created by Keith Weiss on 7/15/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import SwiftUI
import Combine

let IMAGE_SIZE: CGFloat = 100

struct VendorImagesList : View {
    var vendorID: Int64
    var vendorName: String
    @ObservedObject var imageProvider: ImageProvider
    
    var body: some View {
        ZStack {
            if imageProvider.hasData {
                List {
                    ImageSection(imageType: .attribute, images: imageProvider.attributeImages)
                    ImageSection(imageType: .catalog, images: imageProvider.catalogImages)
                    ImageSection(imageType: .marketing, images: imageProvider.marketingImages)
                    ImageSection(imageType: .product, images: imageProvider.productImages)
                }
            }
            else {
                Text("Fetching images for \(self.vendorName).")
                    .font(.headline)
                    .lineLimit(nil)
                    .padding(.top, 40)
                Spacer()
            }
        }
        .navigationBarTitle(Text("\(self.vendorName) Images"))
        .onAppear(perform: {
            self.imageProvider.fetchImageCounts(self.vendorID)
        })
    }
}


struct ImageSection : View {
    var imageType: ImageType
    var images: [DBClientImage]
    
    var body: some View {
        return Section(header: Text(imageType.header)) {
            ForEach(images) { (image: DBClientImage) in
                ImagePathView(image: image)
            }
        }
    }
}


struct ImageModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .frame(width: IMAGE_SIZE, height: IMAGE_SIZE, alignment: .center)
            .clipped()
    }
}


struct ImagePathView : View {
    var image: DBClientImage
    @ObservedObject var imageProvider = ImageProvider()
    
    var body: some View {
        return HStack {
            ZStack {
                Rectangle().frame(width: IMAGE_SIZE, height: IMAGE_SIZE, alignment: .center).foregroundColor(.secondary).cornerRadius(12)
                Image(DBClientImage.gcImage(imageProvider.uiImage, to: Int(IMAGE_SIZE)), scale: CGFloat(1.0), label: Text("Client Asset Image"))
                
                // Image(uiImage: imageService.uiImage ?? UIImage(systemName: "photo")!)
                //   .resizable()
            }
                .modifier(ImageModifier())
            Text(image.imageURLPathComponent?.absoluteString ?? image.name)
                .font(.body)
                .lineLimit(nil)
        }
        .onAppear() {
           self.imageProvider.fetchImage(self.image.cacheURL)
        }
        .onDisappear() {
            self.imageProvider.cancelImage()
        }
    }
}


#if DEBUG
struct VendorImages_Previews : PreviewProvider {
    static var previews: some View {
        NavigationView {
            VendorImagesList(vendorID: 1, vendorName: "Design Design", imageProvider: ImageProvider())
        }
    }
}
#endif
