//
//  VendorList.swift
//  Brandwise Image Downloader
//
//  Created by Keith Weiss on 7/13/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import SwiftUI
import Combine

struct VendorList : View {
    @ObservedObject var imageProvider = ImageProvider()
    
    var body: some View {
        NavigationView {
            List(imageProvider.vendors) { vendor in
                NavigationLink(destination: VendorImagesList(vendorID: vendor.id, vendorName: vendor.name, imageProvider: self.imageProvider)) {
                    HStack {
                        Text(vendor.name)
                        Spacer()
                        Image(systemName: "icloud.and.arrow.down")
                            .onTapGesture {
                                print("You just tapped the cloud.")
                            }
                            .padding()
                    }
                }
            }
            .onAppear() { self.imageProvider.fetchVendors() }
            .navigationBarTitle(Text("Vendor List"))
        }
        .navigationViewStyle(DoubleColumnNavigationViewStyle())
    }
    
    func downloadData(for vendorID: Int64) {
        print("---> VendorID: \(vendorID)")
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        VendorList()
    }
}
#endif
