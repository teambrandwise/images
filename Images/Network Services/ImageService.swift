//
//  ImageService.swift
//  Brandwise Image Downloader
//
//  Created by Keith Weiss on 7/13/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import Foundation
import Combine
import SwiftUI

struct ClientImagesService {
    static var imageCountRequest: URLRequest {
        var request = URLRequest(url: URL(string: "https://bwvmisync.brandwise.com/BWiDataServices/GetImageCounts")!)
        request.httpMethod = "POST"
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue("gzip", forHTTPHeaderField: "Accept-Encoding")
        let imageCountIn = ImageCountIn(vendorList: [])
        request.httpBody = try? JSONEncoder().encode(imageCountIn)
        return request
    }
    
    lazy var vendorListPublisher: AnyPublisher = {
        return imageCountOut(urlRequest: ClientImagesService.imageCountRequest)
    }()
    
    
    func imagesRequest(for vendorID: Int64) -> URLRequest {
        var request = URLRequest(url: URL(string: "https://bwvmisync.brandwise.com/BWiDataServices/GetImageCountsAndImageData_2")!)
        request.httpMethod = "POST"
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue("gzip", forHTTPHeaderField: "Accept-Encoding")
        let vendorTS = VendorTS(vendorID: vendorID)
        let imageCountIn = ImageCountIn(vendorList: [vendorTS])
        request.httpBody = try? JSONEncoder().encode(imageCountIn)
        return request
    }
    
    
    func imageCountOut(urlRequest: URLRequest) -> AnyPublisher<ImageCountOut, Never> {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .custom { decoder in
            let container = try decoder.singleValueContainer()
            let dateString = try container.decode(String.self)
            
            guard !dateString.isEmpty else { return Date() }
            
            if let date = DateFormatter.imageCounts.date(from: dateString) {
                return date
            }
            throw DecodingError.dataCorruptedError(in: container,
                                                   debugDescription: "Cannot decode date string \(dateString)")
        }
        
        
        return URLSession.shared.dataTaskPublisher(for: urlRequest)
            .map({ $0.data })
            .decode(type: ImageCountOut.self, decoder: decoder)
            .catch { error in
                return Just(ImageCountOut.default)
        }
        .receive(on: RunLoop.main)
        .eraseToAnyPublisher()
    }
    
    
    func imagePublisher(url: URL) -> AnyPublisher<UIImage, Never> {
        let photo = UIImage(systemName: "photo") ?? UIImage()
        var request = URLRequest(url: url)
        request.allowsConstrainedNetworkAccess = false
        return URLSession(configuration: .ephemeral, delegate: nil, delegateQueue: OperationQueue.main).dataTaskPublisher(for: request)
            .tryMap({ (result) in
                guard let uiImage = UIImage(data: result.data) else { throw URLError(.badServerResponse) }
                return uiImage
            })
            .replaceError(with: photo)
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
}
