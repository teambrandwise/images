//
//  ImageStore.swift
//  Brandwise Image Downloader
//
//  Created by Keith Weiss on 7/15/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import UIKit
import SwiftUI

final class ImageStore {
    fileprivate typealias _ImageDictionary = [String: [Int: CGImage]]
    fileprivate var images: _ImageDictionary = [:]
    
    fileprivate static var originalSize = 1000
    fileprivate static var scale = 2
    
    static var shared = ImageStore()
    
    func image(image: ClientImage, size: Int) -> Image {
        let index = _guaranteeInitialImage(image: image)
        
        let sizedImage = images.values[index][size]
            ?? _sizeImage(images.values[index][ImageStore.originalSize]!, to: size * ImageStore.scale)
        images.values[index][size] = sizedImage
        
        return Image(sizedImage, scale: Length(ImageStore.scale), label: Text(verbatim: image.id!.uuidString))
    }
    
    static func load(image: ClientImage) -> CGImage {
        guard
            let imageSource = CGImageSourceCreateWithURL(image.serverURL as NSURL, nil),
            let loadedImage = CGImageSourceCreateImageAtIndex(imageSource, 0, nil)
            else {
                fatalError("Couldn't load image \(image.id!.uuidString).jpg from main bundle.")
        }
        return loadedImage
    }
    
    fileprivate func _guaranteeInitialImage(image: ClientImage) -> _ImageDictionary.Index {
        guard let key: String = image.id?.uuidString else { fatalError("ImageStore: Missing uuid for an image.") }
        let index = images.index(forKey: key)
        guard index == nil else { return index! }
        
        let image = ImageStore.load(image: image)
        images[key] = [ImageStore.originalSize: image]
        return images.index(forKey: key)!
    }
    
    fileprivate func _sizeImage(_ image: CGImage, to size: Int) -> CGImage {
        guard
            let colorSpace = image.colorSpace,
            let context = CGContext(
                data: nil,
                width: size, height: size,
                bitsPerComponent: image.bitsPerComponent,
                bytesPerRow: image.bytesPerRow,
                space: colorSpace,
                bitmapInfo: image.bitmapInfo.rawValue)
            else {
                fatalError("Couldn't create graphics context.")
        }
        context.interpolationQuality = .high
        context.draw(image, in: CGRect(x: 0, y: 0, width: size, height: size))
        
        if let sizedImage = context.makeImage() {
            return sizedImage
        } else {
            fatalError("Couldn't resize image.")
        }
    }
}
